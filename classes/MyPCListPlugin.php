<?php

/**
 * Class MyPCListPlugin
 */
class MyPCListPlugin{
    private $wpdb;

    private $connector;
    private $options;
    private $pluginSettings;
    private $cwd;
    private $baseUrl;

    private $defaultOptions = array(
        'api_key' => '',
        'api_token' => '',
        'cache_enabled' => 1,
        'cache_images' => 0,
        'cache_autoupdate_enabled' => 1,
        'cache_autoupdate_interval' => 3600,
        'last_listing_cache' => 0
    );

    /**
     * MyPCListPlugin constructor.
     * @param string $cwd
     * @param string $baseUrl
     */
    public function __construct($cwd, $baseUrl) {
        global $wpdb;

        add_action('plugins_loaded', array(&$this, 'load_textdomain'));
//        $this->load_textdomain();

        $this->wpdb = $wpdb;
        $this->cwd = $cwd;
        $this->baseUrl = $baseUrl;
        $this->dbHandler = new MyPCListDBHandler();
        $this->pluginSettings = new MyPCListPluginSettings($this->cwd, $this->baseUrl, $this->dbHandler, $this->defaultOptions);

        add_filter('query_vars', array(&$this, 'query_vars'));
        add_shortcode("mypclist", array(&$this, 'tag_handler'));

        $this->options = get_option('mpcl-options');
        if(!is_array($this->options)){
            update_option('mpcl-options', $this->defaultOptions);
            $this->options = $this->defaultOptions;
        }

        $this->connector = new MyPCListConnector($this->dbHandler, $this->options['api_key'], $this->options['api_token']);

//        $this->dbHandler->db_init();

        if($this->wpdb->get_var("show tables like 'mpcl_machines'") != 'mpcl_machines' || $this->wpdb->get_var("show tables like 'mpcl_images'") != 'mpcl_images'){
            $this->dbHandler->db_init();
        }
    }

    public function load_textdomain() {
        load_plugin_textdomain( 'mpcl', false, plugin_basename( $this->cwd ).'/languages/' );
    }

    /**
     * Register 'machine_id' query variable
     * @param $qvars
     * @return array
     */
    public function query_vars($qvars){
        $qvars[] = 'machine_id';
        return $qvars;
    }

    /**
     * Handle mypclist shortcode
     * @param $attr
     */
    public function tag_handler($attr){
        $machine_id = get_query_var('machine_id', null);
        if($machine_id === null){
            require_once $this->cwd.'/views/listing-grid.php';
        } else{
            require_once $this->cwd.'/views/single.php';
        }
    }

    /**
     * Get machine entry by remote ID
     * @param $remote_id
     * @return array|bool
     */
    public function mpcl_get_machine($remote_id){
        if(!is_numeric($remote_id)){
            return false;
        }


        if($this->options['cache_enabled']){
            $result = $this->wpdb->get_results("select * from mpcl_machines where remote_id = ".$remote_id." limit 1", ARRAY_A);
            if(!$result || count($result) == 0 || count($result) > 1){
                return false;
            }

            if($this->options['cache_autoupdate_enabled'] && time() - $result[0]['cached'] >= (int)$this->options['cache_autoupdate_interval']){
                $machine = $this->connector->update_machine_single($result[0]['id']);

                if(!is_array($machine)){
                    return false;
                }
            } else{
                $machine = $result[0];
            }

            $machine['photos'] = unserialize($machine['photos']);
        } else{
            $response = $this->connector->request(array(
                'action' => 'getMachine',
                'id' => $remote_id
            ));

            if(!is_array($response) || $response['status'] != 'success'){
                return false;
            }

            $machine = $response['response'];
        }

        return $machine;
    }

    /**
     * Get machine listing
     * @param int $limit
     * @param int $offset
     * @return array|bool|null|object
     */
    public function mpcl_get_machine_listing($limit = 0, $offset = 0){
        if(!is_numeric($limit) || !is_numeric($offset)){
            return false;
        }

        if($this->options['cache_enabled']){
            if($this->options['cache_autoupdate_enabled'] && time() - $this->dbHandler->get_option('last_listing_cache') >= (int)$this->options['cache_autoupdate_interval']){
                // Auto-update machines list
                $this->connector->update_machine_list();
            }

            $result = $this->wpdb->get_results("select * from mpcl_machines order by remote_id desc ".(($limit > 0) ? "limit ".$limit." offset ".$offset : ""), ARRAY_A);
            if(!$result){
                return false;
            }

            if(count($result) > 0){
                foreach($result as $k => $v){
                    $result[$k]['photos'] = unserialize($v['photos']);
                }
            }

            $machines = $result;
        } else{
            $response = $this->connector->request(array(
                'action' => 'getMachinesList',
                'limit' => $limit,
                'offset' => $offset
            ));

            if(!$response || $response['status'] != 'success'){
                return false;
            }

            $machines = $response['response'];

            if(!is_array($machines)){
                return false;
            }
        }

        return $machines;
    }

    /**
     * Get image uri by slug
     * @param $slug
     * @param int $size
     * @return string
     */
    public function mpcl_get_image_uri($slug, $size = 0){
        if(empty($slug)){
            return $this->baseUrl."/img/sample_machine.png";
        }

        if($this->options['cache_enabled']){
            $result = $this->wpdb->get_results("select * from mpcl_images where slug = '".$slug."' limit 1", ARRAY_A);
            if(is_array($result) && count($result)){
                $result[0]['thumb_urls'] = unserialize($result[0]['thumb_urls']);
                if($size > 0 && is_array($result[0]['thumb_urls']) && isset($result[0]['thumb_urls'][$size])){
                    return $result[0]['thumb_urls'][$size];
                } else{
                    return $result[0]['url'];
                }
            }
        }

        $response = $this->connector->request(array(
            'action' => 'getPhoto',
            'slug' => $slug,
            'size' => $size
        ));

        if(is_array($response) && $response['status'] == 'success'){
            $this->wpdb->insert('mpcl_images', array(
                "slug" => $response['response']['slug'],
                "cached" => time(),
                "url" => $response['response']['url'],
                "thumb_urls" => serialize($response['response']['thumb_urls'])
            ));

            if($size > 0 && isset($response['response']['thumb_urls'][$size])){
                return $response['response']['thumb_urls'][$size];
            } else{
                return $response['response']['url'];
            }
        } else{
            return $this->baseUrl."/img/sample_machine.png";
        }
    }

    public function getStateString($state){
        switch($state){
            default: return __("Unknown", "mpcl"); break;
            case '1': return "1/5 - ".__("Broken", "mpcl"); break;
            case '2': return "2/5 - ".__("Needs repair", "mpcl"); break;
            case '3': return "3/5 - ".__("Partially broken", "mpcl"); break;
            case '4': return "4/5 - ".__("Good", "mpcl"); break;
            case '5': return "5/5 - ".__("Very good", "mpcl"); break;
        }
    }
}