<?php

/**
 * Class MyPcListDBHandler
 */
class MyPcListDBHandler{
    private $wpdb;

    /**
     * MyPcListDBHandler constructor.
     */
    public function __construct(){
        global $wpdb;

        $this->wpdb = $wpdb;
    }

    public function get_option($key){
        $key = $this->wpdb->_real_escape($key);
        $result = $this->wpdb->get_results("SELECT * FROM mpcl_options WHERE name='".$key."' LIMIT 1", ARRAY_A);
        if(!is_array($result) || !count($result)) {
            return false;
        }

        return $result[0]['value'];
    }

    public function set_option($key, $value){
        $key = $this->wpdb->_real_escape($key);

        $data = array(
            'name' => $key,
            'value' => $value
        );

        $result = $this->wpdb->get_results("SELECT * FROM mpcl_options WHERE name='".$key."' LIMIT 1", ARRAY_A);
        if(!$result){
            $this->wpdb->insert('mpcl_options', $data);
        } else{
            $this->wpdb->update('mpcl_options',
                $data,
                array(
                    'name' => $key
                )
            );
        }
    }

    /**
     * Insert machine as a new entry into the database
     * @param $machine
     * @return array|bool
     */
    public function insert_machine($machine){
        $data = array(
            'remote_id' => $machine['id'],
            'created' => $machine['created'],
            'cached' => time(),
            'modified' => $machine['modified'],
            'slug' => $machine['slug'],
            'is_visible' => $machine['is_visible'],
            'description' => $machine['description'],
            'physical_state' => $machine['physical_state'],
            'custom_name' => $machine['custom_name'],
            'name' => $machine['name'],
            'manufacturer_id' => $machine['manufacturer_id'],
            'manufacturer' => $machine['manufacturer'],
            'year_of_production' => $machine['year_of_production'],
            'year_of_premiere' => $machine['year_of_premiere'],
            'type_id' => $machine['type_id'],
            'serial_number' => $machine['serial_number'],
            'premiere_price' => $machine['premiere_price'],
            'is_forsale' => $machine['is_forsale'],
            'price' => $machine['price'],
            'is_forreplace' => $machine['is_forreplace'],
            'photos' => $machine['photos'],
            'is_extension' => $machine['is_extension'],
            'is_standalone' => $machine['is_standalone'],
            'type_name' => $machine['type_name']
        );

        foreach($data as $k => $v){
            if(is_array($v)){
                $data[$k] = serialize($v);
            }
        }

        $result = $this->wpdb->insert('mpcl_machines', $data);

        if($result){
            return $data;
        } else{
            return false;
        }
    }

    /**
     * Update machine entry in database
     * @param $machine
     * @return array|bool
     */
    public function update_machine($machine){
        $data = array(
            'remote_id' => $machine['id'],
            'created' => $machine['created'],
            'cached' => time(),
            'modified' => $machine['modified'],
            'slug' => $machine['slug'],
            'is_visible' => $machine['is_visible'],
            'description' => $machine['description'],
            'physical_state' => $machine['physical_state'],
            'custom_name' => $machine['custom_name'],
            'name' => $machine['name'],
            'manufacturer_id' => $machine['manufacturer_id'],
            'manufacturer' => $machine['manufacturer'],
            'year_of_production' => $machine['year_of_production'],
            'year_of_premiere' => $machine['year_of_premiere'],
            'type_id' => $machine['type_id'],
            'serial_number' => $machine['serial_number'],
            'premiere_price' => $machine['premiere_price'],
            'is_forsale' => $machine['is_forsale'],
            'price' => $machine['price'],
            'is_forreplace' => $machine['is_forreplace'],
            'photos' => $machine['photos'],
            'is_extension' => $machine['is_extension'],
            'is_standalone' => $machine['is_standalone'],
            'type_name' => $machine['type_name']
        );

        foreach($data as $k => $v){
            if(is_array($v)){
                $data[$k] = serialize($v);
            }
        }

        $result = $this->wpdb->update('mpcl_machines',
            $data,
            array(
                'remote_id' => $machine['id']
            )
        );

        if($result){
            return $data;
        } else{
            return false;
        }
    }

    /**
     * Initialize SQL tables required for MyPCList integration
     */
    public function db_init(){
        global $wpdb;

        $queries = array("DROP TABLE `mpcl_machines`",
            "CREATE TABLE `mpcl_machines` (
                `id` int(255) NOT NULL,
                `remote_id` int(255) NOT NULL,
                `created` int(32),
                `cached` int(32),
                `modified` int(32),
                `slug` tinytext,
                `is_visible` tinyint(1),
                `description` text,
                `physical_state` tinyint(4),
                `custom_name` tinytext,
                `name` text,
                `manufacturer_id` int(255),
                `manufacturer` text,
                `year_of_production` smallint(6),
                `year_of_premiere` smallint(6),
                `type_id` int(255),
                `serial_number` tinytext,
                `premiere_price` tinytext,
                `is_forsale` tinyint(1),
                `price` tinytext,
                `is_forreplace` tinyint(1),
                `photos` text,
                `is_extension` tinyint(1),
                `is_standalone` tinyint(1),
                `type_name` text
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
            "ALTER TABLE `mpcl_machines` ADD PRIMARY KEY (`id`);",
            "ALTER TABLE `mpcl_machines` MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;",
            "ALTER TABLE `mpcl_machines` ADD UNIQUE(`remote_id`);",
            "DROP TABLE `mpcl_images`;",
            "CREATE TABLE `mpcl_images` (
                `id` int(255) NOT NULL,
                `slug` tinytext,
                `cached` int(32),
                `url` text,
                `thumb_urls` text
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
            "ALTER TABLE `mpcl_images` ADD PRIMARY KEY (`id`);",
            "ALTER TABLE `mpcl_images` MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;",
            "DROP TABLE `mpcl_options`;",
            "CREATE TABLE `mpcl_options` (
                `name` TINYTEXT NOT NULL ,
                `value` TEXT NOT NULL
              )  ENGINE = InnoDB DEFAULT CHARSET=utf8;"
        );

        foreach($queries as $q){
            $wpdb->get_results($q, OBJECT);
        }
    }
}