<?php

/**
 * Class MyPCListConnector
 */
class MyPCListConnector{
    private $wpdb;

    private $host = "http://mypclist.net/index.php";
//    private $host = "http://localhost:8088/index.php";

    private $dbHandler;
    private $key;
    private $token;

    /**
     * MyPCListConnector constructor.
     * @param MyPcListDBHandler $dbHandler
     * @param string $api_key
     * @param string $api_token
     */
    public function __construct(MyPcListDBHandler $dbHandler, $api_key, $api_token){
        global $wpdb;

        $this->wpdb = $wpdb;
        $this->dbHandler = $dbHandler;
        $this->key = $api_key;
        $this->token = $api_token;
    }

    /**
     * Perform an API request to MyPCList server
     * @param $params
     * @return array|bool|mixed|object
     */
    public function request($params){
        if(!strlen($this->key) || !strlen($this->token)){
            return false;
        }

        $params['api_key'] = $this->key;
        $params['api_token'] = $this->token;

        $response = wp_remote_post($this->host, array(
            'timeout' => 30,
            'blocking' => true,
            'body' => $params,
            'cookies' => array()
        ));

        if(is_wp_error($response)){
            return false;
        } else{
            $body = wp_remote_retrieve_body($response);
            $data = json_decode($body, ARRAY_A);
            if(is_array($data)){
                return $data;
            } else{
                return false;
            }
        }
    }

    /**
     * Check server connectivity and authorization by performing a dummy API request
     * @return bool
     */
    public function ping(){
        $response = $this->request(array(
            'action' => 'ping'
        ));

        if(is_array($response) && $response['status'] == 'success'){
            return $response['response'];
        } else{
            return false;
        }
    }

    /**
     * Synchronize local machines database with a remote host
     * @return bool
     */
    public function update_machine_list(){
        // Get current machines list
        $response = $this->request(array(
            'action' => 'getMachinesIDList'
        ));

        if(!is_array($response) || $response['status'] != 'success'){
            return false;
        }


        $ids = $response['response'];

        $result = $this->wpdb->get_results("select id, remote_id from mpcl_machines order by remote_id desc", ARRAY_A);
        if(!is_array($result)){
            $result = array();
        }

        $existing_machines = array();

        // Synchronize machine removals
        foreach($result as $machine){
            if(!in_array($machine['remote_id'], $ids)){
                $this->wpdb->delete('mpcl_machines', array('remote_id' => $machine['id']));
            } else{
                array_push($existing_machines, $machine['remote_id']);
            }
        }

        // Synchronize machines data
        $response = $this->request(array(
            'action' => 'getMachinesList',
            'ids' => $ids
        ));

        if(!is_array($response) || $response['status'] != 'success'){
            return false;
        }

        $machines = $response['response'];

        foreach($machines as $machine){
            if(in_array($machine['id'], $existing_machines)){
                // Update machine
                $this->dbHandler->update_machine($machine);
            } else{
                // Create new machine
                $this->dbHandler->insert_machine($machine);
            }
        }

        $this->dbHandler->set_option('last_listing_cache', time());

        return true;
    }

    /**
     * Synchronize local machine entry with a remote host
     * @param $local_id
     * @return array|bool
     */
    public function update_machine_single($local_id){
        if(!is_numeric($local_id)){
            return false;
        }

        // Get remote ID
        $result = $this->wpdb->get_results("select id, remote_id from mpcl_machines where id = ".$local_id." limit 1", ARRAY_A);
        if(!$result || count($result) == 0){
            return false;
        }

        $remote_id = $result[0]['remote_id'];

        // Synchronize machine data
        $response = $this->request(array(
            'action' => 'getMachine',
            'id' => $remote_id
        ));

        if(!$response){
            return false;
        }

        // Remove machine if cannot synchronize
        if($response['status'] != 'success'){
            $this->wpdb->delete('mpcl_machines', array('id' => $local_id));

            return true;
        }

        $machine = $response['response'];

        return $this->dbHandler->update_machine($machine);
    }
}