��          �   %   �      P     Q     j  	   r     |     �     �     �     �     �  
   �     �     �  �     �   �     E     J     W     d     q     �  D   �     �     �     �  	        
          *     8     I  
   U     `  *   w     �     �     �  2   �        �   >  �   	     �	     �	  	   �	     �	     �	     �	  \   
     h
  	   q
  %   {
     �
                                           
                                       	                                           &laquo; Back to the list API key API token Authorization Broken Cache enabled Cache has been cleared. Clear cache Custom name Data cache Enable cache auto-update Enable image cache Enter your API authorization codes, which can be generated <a target="_blank" href="http://mypclist.net/index.php?p=api-settings">here</a>. They're required to connect with MyPCList server. For less server-to-server bandwith usage you can enable cache of your data. Activating cache will make your blog loading faster. Good Machine type Manufacturer Needs repair Partially broken Physical state Requested machine cannot be found. Check if you entered a valid URL. Unknown Unnamed Update interval (seconds) Very good Project-Id-Version: MyPClist WP
POT-Creation-Date: 2016-03-27 17:08+0200
PO-Revision-Date: 2016-03-27 17:08+0200
Last-Translator: 
Language-Team: MyPClist
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-SearchPath-0: .
 &laquo; Powrót do listy ID klucza API Token klucza API Autoryzacja Zniszczony Włącz pamięć podr. Pamięć podręczna została wyczyszczona. Wyczyść pamięć podręczną Nazwa własna Pamięć podręczna danych Włącz automatyczne aktualizowanie pamięci podr. Włącz pamięć podr. obrazków Wprowadź klucz API w celu autoryzacji. Możesz go wygenerować <a target="_blank" href="http://mypclist.net/index.php?p=api-settings">tutaj</a>. Jest on wymagany przez plugin, by móc pobrać Twoją kolekcję urządzeń. W celu zmniejszenia zużycia transferu na serwerze, możesz włączyć pamięć podręczną danych. Aktywowanie tej opcji spowoduje szybsze ładowanie się bloga. Dobry Rodzaj Producent Wymaga naprawy Cżęściowo uszkodzony Stan fizyczny Żądana maszyna nie została odnaleziona. Upewnij się, czy podany adres URL jest poprawny. Nieznany Bez nazwy Czas pomiędzy odświeżeniami (sek.) Bardzo dobry 