msgid ""
msgstr ""
"Project-Id-Version: MyPClist WP\n"
"POT-Creation-Date: 2016-03-27 17:08+0200\n"
"PO-Revision-Date: 2016-03-27 17:08+0200\n"
"Last-Translator: \n"
"Language-Team: MyPClist\n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c\n"
"X-Poedit-SearchPath-0: .\n"

#: classes/MyPCListPlugin.php:230
msgid "Unknown"
msgstr "Nieznany"

#: classes/MyPCListPlugin.php:231
msgid "Broken"
msgstr "Zniszczony"

#: classes/MyPCListPlugin.php:232
msgid "Needs repair"
msgstr "Wymaga naprawy"

#: classes/MyPCListPlugin.php:233
msgid "Partially broken"
msgstr "Cżęściowo uszkodzony"

#: classes/MyPCListPlugin.php:234
msgid "Good"
msgstr "Dobry"

#: classes/MyPCListPlugin.php:235
msgid "Very good"
msgstr "Bardzo dobry"

#: classes/MyPCListPluginSettings.php:39
msgid "Authorization"
msgstr "Autoryzacja"

#: classes/MyPCListPluginSettings.php:41
msgid "API key"
msgstr "ID klucza API"

#: classes/MyPCListPluginSettings.php:42
msgid "API token"
msgstr "Token klucza API"

#: classes/MyPCListPluginSettings.php:44
msgid "Data cache"
msgstr "Pamięć podręczna danych"

#: classes/MyPCListPluginSettings.php:46
msgid "Cache enabled"
msgstr "Włącz pamięć podr."

#: classes/MyPCListPluginSettings.php:47
msgid "Enable image cache"
msgstr "Włącz pamięć podr. obrazków"

#: classes/MyPCListPluginSettings.php:48
msgid "Enable cache auto-update"
msgstr "Włącz automatyczne aktualizowanie pamięci podr."

#: classes/MyPCListPluginSettings.php:49
msgid "Update interval (seconds)"
msgstr "Czas pomiędzy odświeżeniami (sek.)"

#: classes/MyPCListPluginSettings.php:56
msgid "Cache has been cleared."
msgstr "Pamięć podręczna została wyczyszczona."

#: classes/MyPCListPluginSettings.php:102
msgid ""
"Enter your API authorization codes, which can be generated <a target=\"_blank"
"\" href=\"http://mypclist.net/index.php?p=api-settings\">here</a>. They're "
"required to connect with MyPCList server."
msgstr ""
"Wprowadź klucz API w celu autoryzacji. Możesz go wygenerować <a target="
"\"_blank\" href=\"http://mypclist.net/index.php?p=api-settings\">tutaj</a>. "
"Jest on wymagany przez plugin, by móc pobrać Twoją kolekcję urządzeń."

#: classes/MyPCListPluginSettings.php:106
msgid ""
"For less server-to-server bandwith usage you can enable cache of your data. "
"Activating cache will make your blog loading faster."
msgstr ""
"W celu zmniejszenia zużycia transferu na serwerze, możesz włączyć pamięć "
"podręczną danych. Aktywowanie tej opcji spowoduje szybsze ładowanie się "
"bloga."

#: views/listing-grid.php:47 views/single.php:17
msgid "Unnamed"
msgstr "Bez nazwy"

#: views/options.php:7
msgid "Clear cache"
msgstr "Wyczyść pamięć podręczną"

#: views/single.php:10
msgid "&laquo; Back to the list"
msgstr "&laquo; Powrót do listy"

#: views/single.php:21
msgid "Manufacturer"
msgstr "Producent"

#: views/single.php:27
msgid "Machine type"
msgstr "Rodzaj"

#: views/single.php:33
msgid "Custom name"
msgstr "Nazwa własna"

#: views/single.php:38
msgid "Physical state"
msgstr "Stan fizyczny"

#: views/single.php:64
msgid "Requested machine cannot be found. Check if you entered a valid URL."
msgstr ""
"Żądana maszyna nie została odnaleziona. Upewnij się, czy podany adres URL "
"jest poprawny."
