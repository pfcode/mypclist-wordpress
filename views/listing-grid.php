<?php

if(isset($attr['columns']) && $attr['columns'] > 0 && $attr['columns'] <= 16){
    $column_amount = $attr['columns'] + 1;
} else{
    $column_amount = 4;
}

$column_width = (100 / (int)$column_amount)."%";
//$entries_per_page = $column_amount * 2;
$entries_per_page = 999999;
$page_offset = 0;

$listing = $this->mpcl_get_machine_listing($entries_per_page, $page_offset);

?>

<div class="mpcl-listing mpcl-listing-grid">
    <table>
        <thead>
        <?php for($i = 0; $i < $column_amount; $i++){ ?>
            <td style="width: <?php esc_attr_e($column_width); ?>"></td>
        <?php } ?>
        </thead>
        <?php $listing_count = count($listing); ?>
        <?php for($i = 0; $i < $listing_count; $i+=$column_amount) { ?>
            <tbody class="thumbnails">
            <tr>
                <?php for($j = 0; $j < $column_amount && $listing_count > $j + $i; $j++){ ?>
                    <?php $machine = $listing[$j + $i]; ?>
                    <td class="entry">
                        <div class="thumbnail">
                            <a href="<?php echo esc_url(add_query_arg('machine_id', $machine['remote_id'])); ?>">
                                <img src="<?php echo esc_url($this->mpcl_get_image_uri(count($machine['photos']) ? $machine['photos'][0] : '', 300)); ?>"/>
                            </a>
                        </div>
                    </td>
                <?php } ?>
            </tr>
            </tbody>
            <tbody class="captions">
            <tr>
                <?php for($j = 0; $j < $column_amount && $listing_count > $j + $i; $j++){ ?>
                    <?php $machine = $listing[$j + $i]; ?>
                    <td class="entry">
                        <a href="<?php echo esc_url(add_query_arg('machine_id', $machine['remote_id'])); ?>">
                            <h5><?php esc_attr_e(!empty($machine['name']) ? $machine['name'] : __("Unnamed", "mpcl")); ?></h5>
                        </a>
                    </td>
                <?php } ?>
            </tr>
            </tbody>
        <?php } ?>
    </table>
    <!---->
    <!--    <div class="pagination">-->
    <!--        <div class="page-prev">-->
    <!--            <a href="#">&laquo; --><?php //_e("Previous page", "mpcl"); ?><!--</a>-->
    <!--        </div>-->
    <!--        <div class="page-next">-->
    <!--            <a href="#">--><?php //_e("Next page", "mpcl"); ?><!-- &raquo;</a>-->
    <!--        </div>-->
    <!--    </div>-->
</div>