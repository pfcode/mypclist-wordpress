<?php $machine = $this->mpcl_get_machine($machine_id); ?>

<?php
$parser = new JBBCode\Parser();
$parser->addCodeDefinitionSet(new JBBCode\DefaultCodeDefinitionSet());
?>

<?php if(is_array($machine)){ ?>
    <div class="mpcl-single">
        <a href="<?php echo esc_url(remove_query_arg('machine_id')); ?>" class="trigger-back"><?php _e("&laquo; Back to the list", "mpcl"); ?></a>
        <table style="width: 100%">
            <tr class="head">
                <td style="width: 33.333%">
                    <img class="thumbnail" src="<?php echo esc_url($this->mpcl_get_image_uri(count($machine['photos']) ? $machine['photos'][0] : '', 300)); ?>"/>
                </td>
                <td style="width: 66.666%">
                    <h2><?php echo (!empty($machine['name']) ? esc_html($machine['name']) : __("Unnamed", "mpcl")); ?></h2>
                    <table>
                        <?php if(!empty($machine['manufacturer'])){ ?>
                            <tr>
                                <td><?php _e("Manufacturer", "mpcl"); ?></td>
                                <td><?php esc_html_e($machine['manufacturer']); ?></td>
                            </tr>
                        <?php } ?>
                        <?php if(!empty($machine['type_name'])){ ?>
                            <tr>
                                <td><?php _e("Machine type", "mpcl"); ?></td>
                                <td><?php esc_html_e($machine['type_name']); ?></td>
                            </tr>
                        <?php } ?>
                        <?php if(!empty($machine['custom_name'])){ ?>
                            <tr>
                                <td><?php _e("Custom name", "mpcl"); ?></td>
                                <td><?php esc_html_e($machine['custom_name']); ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td><?php _e("Physical state", "mpcl"); ?></td>
                            <td><?php esc_html_e($this->getStateString($machine['physical_state'])); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="description">
                <td colspan="3">
                    <?php $parser->parse($machine['description']);
                    $html = $parser->getAsHtml();
                    //            $html = nl2br($html);
                    $pattern = array("\\r\\n", "\n", "\t", '  ', '  ');
                    $replace = array('<br />', '<br />', '&#160; &#160; ', '&#160; ', ' &#160;');
                    $html = str_replace($pattern, $replace, $html);
                    echo $html; ?>
                </td>
            </tr>
            <?php if(count($machine['photos']) > 0){ ?>
                <tr class="photos">
                    <td colspan="3">
                        <div class="mpcl-baguette-list">
                            <?php foreach($machine['photos'] as $photo){ ?>
                                <a class="single-photo" href="<?php echo esc_url($this->mpcl_get_image_uri($photo)); ?>" target="_blank">
                                    <img class="thumbnail" src="<?php esc_attr_e($this->mpcl_get_image_uri($photo, 100)); ?>"/>
                                </a>
                            <?php } ?>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
<?php } else{ ?>
    <h5><?php _e("Requested machine cannot be found. Check if you entered a valid URL.", "mpcl"); ?></h5>
<?php } ?>
